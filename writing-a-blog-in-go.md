This is a blog written in Go.

The source code is available [here][repo].

It's small and stupid, and I just wanted to see if it was possible to write
something like this in a few hours. It uses mustache templates and markdown
syntax.

It wasn't, at my first attempt, because I needed to learn how the language
worked... But I rewrote it, and it took about 90 minutes.

And boy, does it ever work fast. For 1000 pages:

```
real    0m0.666s
user    0m0.236s
sys     0m0.422s
```

That's with *zero optimisation* (no concurrency, templates are re-opened,
etc...)

It still only renders individual posts, like this one. But it's a start. It's
easily fast enough to add as an AutoCmd in a .vimrc, for example.

## TODO

- allow front matter in markdown files, with summary, tags
- pull in specific CSS files, if they exist
- render index (latest 5 post summaries)
- render tag summaries
- render month summaries
- link to archives and tags on all pages
- reuse templates
- render pages in parallel
- render posts according to schema.org
- include the author in posts
- allow general configuration (blogo.json?)
- add an init command
- add a 

Go is very nice.

[repo]: https://bitbucket.org/bjjb/blogo
